// Crie uma tela para exibir um contador:

//     armazene o contador em um estado;

//     crie dois botões, um para incrementar e outro para decrementar o contador.

import { useState } from "react";

export default function RefContador() {
  const [contador, setContador] = useState(0);

    function incrementar() {
      setContador(contador +1);
    }
    function mostrarContador() {
      alert('Incrementações: ' + contador)
    }
    function decrementar() {
      setContador(contador -1)
    }

    return (
      <div>
        <h1>Clique para incrementar ou decrementar</h1>
        <p>Contador: {contador}</p>
        <button onClick={incrementar}>Incrementar</button>
        <button onClick={mostrarContador}></button>
        <button onClick={decrementar}></button>
      </div>

    );
}