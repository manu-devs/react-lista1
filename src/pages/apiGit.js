import React, { useState, useEffect } from 'react';


async function getUserData() {

    const [vetor, setVetor] = useState([])
    const [status, setStatus] = useState('Loading...')
  useEffect(() => {
    obterDados();
  });

  const obterDados = async () =>{

  const dados = await fetch(`https://api.github.com/users/${username}`);
    const converter = await dados.json();
    setVetor(converter);
    setStatus('Dados do usuário:');
      console.log(dados)

        return(
          <div>
          <ul>
          {vetor.map(objeto => (<li>{objeto.login}</li>))}
          {vetor.map(objeto => (<li>{objeto.name}</li>))}


          </ul>
        </div>
        )

        }    
       
      };
function BuscaGit(){
useEffect(() => {
  getUserData({fkbral})
}, [])

}
BuscaGit();

// Resolução: https://www.youtube.com/watch?v=svgWCzk3Jdk
// 6:
// Crie uma página para exibir as informações de um usuário do Github.

// API do GitHub: https://api.github.com/users/[seuUsuario]

// onde [seuUsuario] é o seu nome de usuário, exemplo: DaviGn, fkbral

//     armazene as informações (nome, e-mail, etc) em um estado e após carregados, exiba-os em tela.

//     crie um estado de loading para mostrar um carregamento enquanto aguarda o retorno da API do Github.

//     liste os repositórios deste usuário. Armazene esta listagem em outro estado e crie um loading para o mesmo, 
//     semelhante ao tópico 2.

// API do GitHub: https://api.github.com/users/[seuUsuario]/repos

//     Adicional: coloque um console.log no fluxo de renderização do componente e observe quantas vezes é 
//     dado print :)