
import React from 'react';
// import ReactDOM from 'react-dom';

// const green = '#39D1B4';
// const yellow = '#FFD712';

// export default class Toggle extends React.Component{
//   constructor(props){
//     super(props);
//     this.state = { color: green };
//     this.changeColor = this.changeColor.bind(this);
//   }
//   changeColor(){
//     const newColor = this.state.color == green ? yellow : green;
//     this.setState({ color: newColor })
//   }
//   render(){
//     return(
//       <div style={{background: this.state.color}}>
//       <h1>Change my color</h1>
//       <button onClick={this.changeColor}>Click</button>
//       </div>
//     )
//   }
// }
// ReactDOM.render(<Toggle />, document.getElementById('root'))














// Crie um toggle para alterar entre light/dark mode de um site

// //     crie uma página que terá um estado chamado theme que receberá o valor ‘light’ ou ‘dark’ 
//     (esta página será pai das demais partes);

//     crie um botão para alternar entre as duas opções;

//     crie 1 ou mais seções nesta página em componentes separados (exemplo: banner, header);

//     repasse o valor do theme para estes componentes via props e renderize com cores diferentes 
//     utilizando a propriedade “style” ou “className”.

//     Exemplo: background: (theme === ‘light’ ? ‘#fff’ : ‘#000’)

// 8:
// Continuando o exercício anterior, crie um componente Menu para esta página.

//     os itens do menu deverão ser: cadastros, financeiro, relatórios;

//     crie um novo botão na página principal (no componente pai) similar ao que modifica o tema, 
//     mas desta vez para alternar o tipo de usuário entre ‘admin’ ou ‘user’ e salve isto em um estado;

//     repasse o valor deste estado para o componente Menu via props;

//     um usuário do tipo ‘user’ não poderá visualizar os menus ‘cadastros’ e ‘financeiro’, sendo assim, 
//     utilize o valor do tipo de usuário recebido via propriedade para fazer esta verificação.
