// 3:
// De acordo com o guia presente em https://reactrouter.com/docs/en/v6/getting-started/installation#basic-installation, 
// instale o react-router-dom e crie uma pasta pages para organizar nossa aplicação em páginas. 
// Faça uma página Home (”/”) com links para as outras páginas, outra Gallery (”/galeria”) para exibir 
// o componente ImageGallery e mais uma de Chat (”/chat”) para exibir os componentes ChatBaloon 
// criados no exercício anterior.
import React from "react";
import { Route, BrowserRouter } from "react-router-dom";
import { Link } from 'react-router-dom'


export function Home() {
  
    return (
      <div>
        <ul>
          <li>
            <Link to="/Home">Home</Link>
          </li>
  
          <li>
            <Link to="/Gallery">Gallery</Link>
          </li>
  
          <li>
            <Link to="/ImageGallery">Image</Link>
          </li>
  
          <li>
            <Link to="/Chat">Chat</Link>
          </li>
        </ul>
    </div>
    )
}
export default Home
