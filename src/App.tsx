import React from 'react';
import './App.css';

const imagens = [
  {
    id: `01`,
    Imagem: "https://source.unsplash.com/random/?city,night"
  },
  {
    id:`02`,
    Imagem: "https://source.unsplash.com/random/?city,night"
  },
  {
    id:`03`,
    Imagem: "https://source.unsplash.com/random/?city,night"
  },
  {
    id: `04`,
    Imagem: "https://source.unsplash.com/random/?city,night"
  },
  {
    id: `05`,
    Imagem: "https://source.unsplash.com/random/?city,night"
  },
];

function App() {
  return (
    <header className="App-header">
    <div> App</div>
      <img
        src="https://bobbyhadz.com/images/blog/react-prevent-multiple-button-clicks/thumbnail.webp"
        alt="imagemreact"
      />
    <div  style={{width: '180px', display: 'flex'}}>
    {imagens.map(({ id, Imagem }) => (
     <div key={id}>
     <div>
       <img key={id} src={Imagem} alt='Cidade' />
     </div>
     
   </div>
   ))}
   </div>
      </header>                                 
    
  
    
  );
}

export default App;




// Dentro do arquivo App.tsx, crie uma varíavel para armazenar a url desta imagem 
// (https://images.unsplash.com/photo-1633356122102-3fe601e05bd2?ixlib=rb-1.2.1&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=870&q=80https://images.unsplash.com/photo-1633356122102-3fe601e05bd2?ixlib=rb-1.2.1&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=870&q=80) e utilize seu valor para exibir uma imagem em tela com 180px de largura.

// Agora crie uma lista com 12 URLs de imagens e as exiba em tela fazendo um map nesta lista.

//     Dica: todas urls podem ser iguais com este valor 
//     (https://source.unsplash.com/random/?city,night) para gerar imagens aletórias de um tema 
//     específico.

// Estilize as imagens para exibir 3 por linha usando display: flex
//
// 3:
// De acordo com o guia presente em https://reactrouter.com/docs/en/v6/getting-started/installation#basic-installation, 
// instale o react-router-dom e crie uma pasta pages para organizar nossa aplicação em páginas. 
// Faça uma página Home (”/”) com links para as outras páginas, outra Gallery (”/galeria”) para exibir 
// o componente ImageGallery e mais uma de Chat (”/chat”) para exibir os componentes ChatBaloon 
// criados no exercício anterior.
// 4:
// Crie uma tela para exibir um contador:

//     armazene o contador em um estado;

//     crie dois botões, um para incrementar e outro para decrementar o contador.






